# LaravelAPIs

Creacion de una API Restful con el Framework de Laravel

Modelo de Datos
=================
#### Tablas:

- Alumno (Inscrito)
    - ID
    - Nombres
    - Apellidos
    - Correo Electrónico
    - Estado del pago (Pagado, pendiente)
    - Residente Peruano
    - Asistencia
    - Número de teléfono
    - Empresa (ID)
- Empresa
    - ID
    - Nombre (string)
    - Descripción (string)
    - Contacto (Numero de telefono) (string)
    - created_at
    - updated_at

- Precio
    - ID
    - Tipo (Becado, pre-venta, regular)
    - Costo
    - Activo
- Pago (Registro/Inscripción)
    - Inscrito (ID)
    - Precio (ID)
    - Observación


Instalación
===============

Para MySQL 8.0, tube problemas de conexion para los migration, buscando encontre que hay que reconstruir PHP, el problema es solo con la ultima version, para poder conectarse.
Se necesita crear un usuario y a la vez otorgarle todos los privilegios

Para poder solucionarlo tenemos que ejecutar estos comandos:
~~~~sql
CREATE USER 'usuario'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';

GRANT ALL PRIVILEGES ON *.* TO 'usuario'@'localhost' WITH GRANT OPTION;
~~~~


**Crear BD edcamp**

~~~~sql
CREATE DATABASE edcamp;
~~~~