<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;

use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    //Agregamos este metodo render
    public function render($request, Throwable $e){
        
        // dd($e);
        // if($e instanceof ModelNotFoundException )

        //Comparacion de la clase
        if(get_class($e) === 'Illuminate\Database\Eloquent\ModelNotFoundException')
        {
            return response()->json([
                "message" => "No se a encontrado el dato solicitado",
                "status" => Response::HTTP_NOT_FOUND,     
            ], Response::HTTP_NOT_FOUND);
        }
        return parent::render($request, $e);
    }
}
