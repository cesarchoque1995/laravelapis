<?php

namespace App\Http\Controllers;

use App\Models\Alumno;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\DB;

class AlumnoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alumnos =  Alumno::all();

        return response()->json([
            "data" => $alumnos,
            "status" => Response::HTTP_OK
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alumnos = Alumno::create($request->all());
        
        return response()->json([
            "message" => "El Alumno $alumnos[name] $alumnos[lastname] ha sido creado correctamente",
            "data" => $alumnos,
            "status" => Response::HTTP_CREATED,
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function show(Alumno $alumno)
    {
        $empresa = Empresa::findOrFail($alumno->idCompany);
                            
        // dd($dataAlumno);
        return response()->json([
            "message" => "Los datos del Alumno $alumno[name] se muestran correctamente",
            "empresa" => $empresa,
            "alumno" => $alumno,
            "status" => Response::HTTP_OK,
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function edit(Alumno $alumno)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Alumno $alumno)
    {
        $alumno -> update($request->all());
        
        return response()->json([
            "message" => "Los datos del Alumno han sido modificado correctamente",
            "data" => $alumno,
            "status" => Response::HTTP_OK,
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Alumno  $alumno
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alumno $alumno)
    {
        $alumno->delete();

        return response()->json([
            "message" => "El Alumno ha sido eliminado correctamente",
            "data" => $alumno,
            "status" => Response::HTTP_OK,
        ], Response::HTTP_OK);
    }
}
