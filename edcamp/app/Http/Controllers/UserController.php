<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class UserController{
    public function login(){

        $data = [
            'email' => request('email'),
            'password' => request('password') 
        ];

        //Este metodo permite ejecutar un login al usuario
        if(Auth::attempt($data)){
            return response()->json("Bienvenido", 200);
        }else{
            return response()->json("Error en el Login", 401);
        }
    }
}