<?php

namespace App\Http\Controllers;


use App\Models\Precio;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PrecioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Listar todos los precios

        $precios =  Precio::all();

        return response()->json([
            "data" => $precios,
            "status" => Response::HTTP_OK
        ], Response::HTTP_OK);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Metodo para guardar en la BD
        $precio = Precio::create($request->all());
        
        return response()->json([
            "message" => "El precio ha sido creado correctamente",
            "data" => $precio,
            "status" => Response::HTTP_CREATED,
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Precio  $precio
     * @return \Illuminate\Http\Response
     */
    public function show(Precio $precio)
    {
        
        // return $precio;

        return response()->json([
            "message" => "El precio $precio[id] se muestra correctamente",
            "data" => $precio,
            "status" => Response::HTTP_OK,
        ], Response::HTTP_OK);

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Precio  $precio
     * @return \Illuminate\Http\Response
     */
    public function edit(Precio $precio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Precio  $precio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Precio $precio)
    {
        $precio -> update($request->all());
        
        return response()->json([
            "message" => "El precio ha sido modificado correctamente",
            "data" => $precio,
            "status" => Response::HTTP_OK,
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Precio  $precio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Precio $precio)
    {
        //Eliminar un precio en base al id
        $precio->delete();

        return response()->json([
            "message" => "El precio ha sido eliminado correctamente",
            "data" => $precio,
            "status" => Response::HTTP_OK,
        ], Response::HTTP_OK);
    }
}
