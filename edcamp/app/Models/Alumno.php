<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $fillable = [
        "name",
        "lastname",
        "email",
        "state",
        "peruvian",
        "assistance",
        "phone",
        "idCompany"
    ];

    //Creaos una fucncion para el atributo foraneo
    public function company(){
        return $this->belongsTo('App\Models\Empresa', 'idCompany');
    }
}
