<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $fillable = [
        "idStudent",
        "idCost",
    ];

    //Creaos una fucncion para el atributo foraneo
    public function student(){
        return $this->belongsTo('App\Models\Alumno', 'idStudent');
    }

    //Creaos una fucncion para el atributo foraneo
    public function cost(){
        return $this->belongsTo('App\Models\Alumno', 'idCost');
    }
}
